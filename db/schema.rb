# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160421015036) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "concerts", force: :cascade do |t|
    t.string   "location"
    t.date     "date"
    t.integer  "orchestra_id"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  add_index "concerts", ["orchestra_id"], name: "index_concerts_on_orchestra_id", using: :btree

  create_table "conductors", force: :cascade do |t|
    t.string   "first_name"
    t.string   "last_name"
    t.string   "email"
    t.date     "date_joined"
    t.integer  "orchestra_id"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
    t.string   "photo_url"
  end

  add_index "conductors", ["orchestra_id"], name: "index_conductors_on_orchestra_id", using: :btree

  create_table "managers", force: :cascade do |t|
    t.string   "first_name"
    t.string   "last_name"
    t.string   "email"
    t.date     "date_joined"
    t.integer  "orchestra_id"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  add_index "managers", ["orchestra_id"], name: "index_managers_on_orchestra_id", using: :btree

  create_table "musicians", force: :cascade do |t|
    t.string   "first_name"
    t.string   "last_name"
    t.string   "email"
    t.string   "instrument"
    t.boolean  "is_section_leader", default: false
    t.date     "date_joined"
    t.datetime "created_at",                        null: false
    t.datetime "updated_at",                        null: false
    t.integer  "section_id"
  end

  add_index "musicians", ["section_id"], name: "index_musicians_on_section_id", using: :btree

  create_table "orchestras", force: :cascade do |t|
    t.string   "title"
    t.string   "kind"
    t.string   "location"
    t.date     "date_founded"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
    t.integer  "user_id"
  end

  create_table "sections", force: :cascade do |t|
    t.string   "title"
    t.integer  "orchestra_id"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  create_table "users", force: :cascade do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

  add_foreign_key "concerts", "orchestras"
  add_foreign_key "conductors", "orchestras"
  add_foreign_key "managers", "orchestras"
end
