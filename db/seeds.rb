# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

Orchestra.destroy_all
Orchestra.create!([{
  title: "Boston Symphony Orchestra",
  kind: "Symphony",
  location: "Boston, MA",
  date_founded: "1881/10/22"
},
{
  title: "Chicago Symphony Orchestra",
  kind: "Symphony",
  location: "Chicaco, IL",
  date_founded: "1891/10/16"
},
{
  title: "New York Philharmonic",
  kind: "Symphony",
  location: "New York City, NY",
  date_founded: "1842/12/2"
}])

Section.destroy_all
Section.create!([{
  title: "Strings",
},
{
  title: "Brass",
},
{
  title: "Percussion"
}])

Musician.destroy_all
Musician.create!([{
  first_name: "Art",
  last_name: "Garfunkel",
  email: "art@example.com",
  instrument: "guitar",
  is_section_leader: false,
  date_joined: "1970/7/2"
},
{
  first_name: "Kanye",
  last_name: "West",
  email: "kanye@example.com",
  instrument: "choir",
  is_section_leader: false,
  date_joined: "2004/1/8"
},
{
  first_name: "Louis",
  last_name: "Armstrong",
  email: "louis@example.com",
  instrument: "trumpet",
  is_section_leader: false,
  date_joined: "1925/11/12"
}])

puts "Everything got made!"
