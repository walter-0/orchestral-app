class AddMusiciansToSections < ActiveRecord::Migration
  def change
    add_reference :musicians, :section, index: true
  end
end
