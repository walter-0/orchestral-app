class ChangeTypeToKind < ActiveRecord::Migration
  def change
    change_table :orchestras do |t|
      t.rename :type, :kind
    end
  end
end
