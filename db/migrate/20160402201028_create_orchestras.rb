class CreateOrchestras < ActiveRecord::Migration
  def change
    create_table :orchestras do |t|
      t.string :title
      t.string :type
      t.string :location
      t.date :date_founded

      t.timestamps null: false
    end
  end
end
