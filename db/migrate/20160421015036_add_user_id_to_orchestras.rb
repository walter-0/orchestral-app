class AddUserIdToOrchestras < ActiveRecord::Migration
  def change
    add_column :orchestras, :user_id, :integer
  end
end
