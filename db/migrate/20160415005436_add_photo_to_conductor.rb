class AddPhotoToConductor < ActiveRecord::Migration
  def change
    add_column :conductors, :photo_url, :string
  end
end
