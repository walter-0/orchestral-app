class CreateConcerts < ActiveRecord::Migration
  def change
    create_table :concerts do |t|
      t.string :location
      t.date :date
      t.references :orchestra, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
