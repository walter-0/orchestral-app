class CreateConductors < ActiveRecord::Migration
  def change
    create_table :conductors do |t|
      t.string :first_name
      t.string :last_name
      t.string :email
      t.date :date_joined
      t.references :orchestra, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
