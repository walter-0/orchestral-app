class CreateMusicians < ActiveRecord::Migration
  def change
    create_table :musicians do |t|
      t.string :first_name
      t.string :last_name
      t.string :email
      t.string :instrument
      t.boolean :is_section_leader, default: false
      t.date :date_joined


      t.timestamps null: false
    end
  end
end
