require 'test_helper'

class MusicianTest < ActiveSupport::TestCase

 setup do
   @musician = Musician.new
   @section = Section.new
 end

 test "musician should have a first and last name" do
   if @musician.first_name == nil && @musician.last_name == nil
     assert_not @musician.save, "musician was saved without a name"
   end
 end

 # test "musician should have a valid email" do
 #   if condition
 #
 #   end
 # end

 test "musician should have an instrument" do
   if @musician.instrument == nil
     assert_not @musician.save, "musician was saved without an instrument"
   end
 end

 # test "musicians should not be able to change their own section leader status" do
 #
 # end

 test "musician should have a date joined" do
   if @musician.date_joined == nil
     assert_not @musician.save, "musician was saved without a date joined"
   end
 end

 # test "no section may have more than 1 section leader" do
 #   @musician.section_id = @section.id
 #   if @
 #
 #   end
 # end

end
