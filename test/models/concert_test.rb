require 'test_helper'

class ConcertTest < ActiveSupport::TestCase

  setup do
    @concert = Concert.new
  end

  test "concert should have a date" do
    if @concert.date == nil
      assert_not @concert.save, "concert was saved without a date"
    end
  end

  test "concert should have a location" do
    if @concert.location == nil
      assert_not @concert.save, "concert was saved without a location"
    end
  end

end
