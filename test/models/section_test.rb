require 'test_helper'

class SectionTest < ActiveSupport::TestCase

  def setup
    @section = Section.new
  end

  test "section should have a title" do
    if @section.title == nil
      assert_not @section.save, "Saved a section without a title"
    end
  end

  test "section should belong to an orchestra" do
    if @section.orchestra_id == nil
      assert_not @section.save, "Saved a section that has no orchestra"
    end
  end

end
