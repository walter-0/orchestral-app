require 'test_helper'

class OrchestraTest < ActiveSupport::TestCase

  setup do
    @orchestra = Orchestra.new
  end

  test "orchestra should have a title" do
    if @orchestra.title == nil
      assert_not @orchestra.save, "Saved an orchestra without a title"
    end
  end

  test "orchestra should have a location" do
    if @orchestra.location == nil
      assert_not @orchestra.save, "Saved an orchestra without a location"
    end
  end

  test "orchestra should have a date founded" do
    if @orchestra.date_founded == nil
      assert_not @orchestra.save, "Saved an orchestra without a date founded"
    end
  end
end
