require 'test_helper'

class ConductorTest < ActiveSupport::TestCase

  setup do
    @orchestra = Orchestra.create({
      title: "title",
      location: "location",
      kind: "kind",
      date_founded: "1999/12/31"
    })
    @previous_conductor = Conductor.create({
      first_name: "firstname",
      last_name: "lastname",
      email: "email@example.com",
      date_joined: "1999/12/3",
      orchestra_id: @orchestra.id
    })
    @conductor = Conductor.new
  end

  test "conductor should have a first and last name" do
    if @conductor.first_name == nil && @conductor.last_name == nil
      assert_not @conductor.save, "conductor was saved without a name"
    end
  end

  # test "conductor should have a valid email" do
  #   if condition
  #
  #   end
  # end


  test "conductor should have a date joined" do
    if @conductor.date_joined == nil
      assert_not @conductor.save, "conductor was saved without a date joined"
    end
  end

  test "there should never be more than 1 conductor per orchestra" do
    if @orchestra.conductor != nil
      assert_not @conductor.orchestra_id = @orchestra.id, "conductor was added to an orchestra with a conductor already"
    end
  end


end
