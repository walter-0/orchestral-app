class Orchestra < ActiveRecord::Base
  validates :title,
            :location,
            :date_founded, presence: true

  has_many :sections, dependent: :destroy
  has_many :musicians, through: :sections
  has_many :concerts, dependent: :destroy
  has_one  :conductor, dependent: :destroy
  
  belongs_to :user
end
