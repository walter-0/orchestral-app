class Section < ActiveRecord::Base
  validates :title, presence: true

  has_many :musicians, dependent: :destroy
  
  belongs_to :orchestra
end
