class Musician < ActiveRecord::Base
  validates :first_name,
            :last_name,
            :instrument,
            :date_joined,
            :email, presence: true
            
  validates :email, format: { with: /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i, on: :create }
  validates :is_section_leader, :uniqueness => { :scope => :section_id }, :if => :is_section_leader

  belongs_to :section
end
