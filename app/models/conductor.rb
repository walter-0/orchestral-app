class Conductor < ActiveRecord::Base
  validates :first_name,
            :last_name,
            :date_joined, presence: true

  validates :email, format: { with: /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i, on: :create }
  validates :orchestra_id, uniqueness: true

  belongs_to :orchestra
end
