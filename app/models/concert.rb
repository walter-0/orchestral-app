class Concert < ActiveRecord::Base
  validates :location,
            :date, presence: true
  
  belongs_to :orchestra
end
