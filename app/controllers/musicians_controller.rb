class MusiciansController < ApplicationController
  before_action :set_musician, only: [:show, :edit, :update, :destroy]
  before_action :set_orchestra
  before_action :set_section

  def show
  end

  def new
    @musician = Musician.new
  end

  def edit
  end

  def create
    @musician = Musician.new(musician_params)
    if @musician.save
      redirect_to orchestra_section_musician_path(@orchestra, @section, @musician), notice: 'Musician was successfully created.'
    else
      render :new
    end
  end

  def update
    if @musician.update(musician_params)
      redirect_to orchestra_section_musician_path(@orchestra, @section, @musician), notice: 'Musician was successfully updated.'
    else
      render :edit
    end
  end

  def destroy
    @musician.destroy
    redirect_to orchestra_section_path(@orchestra, @section), notice: 'Musician was successfully destroyed.'
  end

  private
    def set_musician
      @musician = Musician.find(params[:id])
    end

    def set_section
      @section = Section.find(params[:section_id])
    end

    def set_orchestra
      @orchestra = Orchestra.find(params[:orchestra_id])
    end

    def musician_params
      params.require(:musician).permit(:first_name, :last_name, :email, :instrument, :is_section_leader, :date_joined, :section_id)
    end
end
