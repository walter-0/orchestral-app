class ConcertsController < ApplicationController
  before_action :set_concert, only: [:show, :edit, :update, :destroy]
  before_action :set_orchestra

  def new
    @concert = Concert.new
  end

  def edit
  end

  def create
    @concert = Concert.new(concert_params)
    if @concert.save
      redirect_to orchestra_concert_path(@orchestra, @concert)
    else
      render :new
    end
  end

  def update
    if @concert.update(concert_params)
      redirect_to orchestra_concert_path(@orchestra, @concert)
    else
      render :edit
    end
  end

  def destroy
    @concert.destroy
    redirect_to orchestra_path(@orchestra)
  end

  private
    def set_concert
      @concert = Concert.find(params[:id])
    end

    def set_orchestra
      @orchestra = Orchestra.find(params[:orchestra_id])
    end

    def concert_params
      params.require(:concert).permit(:location, :date, :orchestra_id)
    end
end
