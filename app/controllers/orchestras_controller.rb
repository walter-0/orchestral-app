class OrchestrasController < ApplicationController
  before_action :set_orchestra, only: [:show, :edit, :update, :destroy]
  before_filter :authenticate_user!

  # GET /orchestras
  # GET /orchestras.json
  def index
    @orchestras = Orchestra.where(user_id: current_user.id)
  end

  # GET /orchestras/1
  def show
    @sections = @orchestra.sections.order(:title)
    @concerts = @orchestra.concerts.order(:date)
  end

  # GET /orchestras/new
  def new
    @orchestra = Orchestra.new
  end

  # GET /orchestras/1/edit
  def edit
  end

  # POST /orchestras
  def create
    @orchestra = Orchestra.new(orchestra_params)

    if @orchestra.save
      redirect_to @orchestra, notice: 'Orchestra was successfully created.'
    else
      render :new
    end

  end

  # PATCH/PUT /orchestras/1
  def update
    if @orchestra.update(orchestra_params)
      redirect_to @orchestra, notice: 'Orchestra was successfully updated.'
    else
      render :edit
    end
  end

  # DELETE /orchestras/1
  def destroy
    @orchestra.destroy
    redirect_to orchestras_url, notice: 'Orchestra was successfully destroyed.'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_orchestra
      @orchestra = Orchestra.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def orchestra_params
      params.require(:orchestra).permit(:title, :kind, :location, :date_founded, :user_id)
    end
end
