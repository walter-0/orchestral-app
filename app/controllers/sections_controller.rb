class SectionsController < ApplicationController
  before_action :set_section, only: [:show, :edit, :update, :destroy]
  before_action :set_orchestra

  def show
    @musicians = @section.musicians.all.order(:last_name)
  end

  def new
    @section = Section.new
  end

  def edit
  end

  def create
    @section = Section.new(section_params)
    if @section.save
      redirect_to orchestra_path(@orchestra)
    else
      render :new
    end
  end

  def update
    if @section.update(section_params)
      redirect_to orchestra_section_path(@orchestra, @section), notice: 'Section was successfully updated.'
    else
      render :edit
    end
  end

  def destroy
    @section.destroy
    redirect_to orchestra_path(@orchestra), notice: 'Section was successfully destroyed.'
  end

  private
    def set_section
      @section = Section.find(params[:id])
    end

    def set_orchestra
      @orchestra = Orchestra.find(params[:orchestra_id])
    end

    def section_params
      params.require(:section).permit(:title, :orchestra_id)
    end
end
