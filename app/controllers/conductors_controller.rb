class ConductorsController < ApplicationController
  before_action :set_conductor, only: [:show, :edit, :update, :destroy]
  before_action :set_orchestra

  def show
  end

  def new
    @conductor = Conductor.new
  end

  def edit
  end

  def create
    @conductor = Conductor.new(conductor_params)
    if @conductor.save
      redirect_to orchestra_conductor_path(@orchestra, @conductor), notice: 'Conductor was successfully created.'
    else
      render :new
    end
  end

  def update
    if @conductor.update(conductor_params)
      redirect_to orchestra_path(@orchestra), notice: 'Conductor was successfully updated.'
    else
      render :edit
    end
  end

  def destroy
    @conductor.destroy
    redirect_to orchestra_path(@orchestra), notice: 'Conductor was successfully destroyed.'
  end

  private

    def set_conductor
      @conductor = Conductor.find(params[:id])
    end

    def set_orchestra
      @orchestra = Orchestra.find(params[:orchestra_id])
    end

    def conductor_params
      params.require(:conductor).permit(:first_name, :last_name, :email, :date_joined, :orchestra_id)
    end
end
