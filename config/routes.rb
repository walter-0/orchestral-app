Rails.application.routes.draw do

  devise_for :users, path: "/", path_names: { sign_in: 'login', sign_out: 'logout', password: 'password', registration: 'register', sign_up: 'signup' }


  root to: 'orchestras#index'

  resources :orchestras do
    resources :concerts
    resources :conductors
    resources :sections do
      resources :musicians
    end
  end
end
