# Orchestra
  ## has_many
    sections
    musicians, through: sections
    concerts
  ## has_one
    conductor
    manager
  ## properties
    title
    kind
    location
    date_founded
---
# Section
  ## has_many
    musicians
  ## belongs_to
    orchestra
  # properties
    title
---
# Musician
  ## belongs_to
    section
  ## properties
    first_name
    last_name
    email
    instrument
    is_section_leader
    date_joined
---
# Conductor
  ## belongs_to
    orchestra
  ## properties
    first_name
    last_name
    email
    date_joined
---
# Manager
  ## belongs_to
    orchestra
  ## properties
    first_name
    last_name
    email
    date_joined
---
# Concert
  ## belongs_to
    orchestra
  ## properties
    date
    location
