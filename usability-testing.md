# Harmonize - Orchestra management with style

### Type: Web app

### Elevator pitch: Harmonize is a webapp for users to manage orchestras.

### Features:
- ##### Create orchestras and add sections and musicians
- ##### Optionally assign a musician to be a section leader
- ##### Assign a conductor
- ##### Schedule concerts

### Usability Testing Tasks
- ##### Create one Orchestra and change its info.
- ##### Create one Section and change its info.
- ##### Create two Musicians and change their info, assigning one as section leader.
- ##### Assign a Conductor and change their info.
